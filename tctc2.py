import numpy, os

PLAYER1PIECE = 1
PLAYER2PIECE = 2

def play_again_question():
    answer = input("Play again? Y if Yes, anything else means No: ")
    
    if(answer.lower() == "y"):
        board = numpy.zeros(shape = (3,3))
        return True
    else:
        return False
    

def check_if_valid_input(slot_number):
    if slot_number.isdigit():
        slot_number = int(slot_number)
        if(slot_number > 0 and slot_number < 10):
            return True
        else:
            return False
    else:
        return False

def check_if_valid(board, row, col, piece):
    if board[row][col] == 0:
        board[row][col] = piece
        return True
    else:
        return False

def slot_num(number,board):
    options = {1 : (0,0),
               2 : (0,1),
               3 : (0,2),
               4 : (1,0),
               5 : (1,1),
               6 : (1,2),
               7 : (2,0),
               8 : (2,1),
               9 : (2,2),
              }
    
    return options[number]

def check_if_win(board, piece):
    if(board[0][0] == piece and board[0][1] == piece and board[0][2] == piece):
        return True
    elif(board[0][0] == piece and board[1][1] == piece and board[2][2] == piece):
        return True
    elif(board[0][0] == piece and board[1][0] == piece and board[2][0] == piece):
        return True
    elif(board[1][0] == piece and board[1][1] == piece and board[1][2] == piece):
        return True
    elif(board[2][0] == piece and board[2][1] == piece and board[2][2] == piece):
        return True
    elif(board[2][0] == piece and board[1][1] == piece and board[0][2] == piece):
        return True
    elif(board[0][1] == piece and board[1][1] == piece and board[2][1] == piece):
        return True
    elif(board[0][2] == piece and board[1][2] == piece and board[2][2] == piece):
        return True
    else:
        return False

def draw_board(board):
    os.system('cls')
    print(' ' + disguise_piece(board[0][0]) + ' | ' + disguise_piece(board[0][1]) + ' | ' + disguise_piece(board[0][2]))
    print('-----------')
    print(' ' + disguise_piece(board[1][0]) + ' | ' + disguise_piece(board[1][1]) + ' | ' + disguise_piece(board[1][2]))
    print('-----------')
    print(' ' + disguise_piece(board[2][0]) + ' | ' + disguise_piece(board[2][1]) + ' | ' + disguise_piece(board[2][2]))
    print('\n' * 5)

def disguise_piece(element):
    if(element == 0):
        return " "
    elif(element == 1):
        return "X"
    else:
        return "O"

board = numpy.zeros(shape = (3,3))

running = True

turn = 1

while running:
    is_input_valid = False
    if(turn % 2 != 0): #player 1
        while not is_input_valid:
            draw_board(board)
            answer = input("Choose which slot to put your piece player 1 (1-9): ")
            verdict = check_if_valid_input(answer)
            if verdict == True:
                slot = int(answer)
                is_input_valid = True

        coords = slot_num(slot,board)
        proceed = check_if_valid(board, coords[0], coords[1], PLAYER1PIECE)
        
        if(check_if_win(board, PLAYER1PIECE)):
            draw_board(board)
            print("Player 1 Won!")
            running = play_again_question()
            board = numpy.zeros(shape = (3,3))
            
        else:         
            if proceed:
                    turn += 1
    if(turn % 2 == 0): #player 2
        while not is_input_valid:
            draw_board(board)
            answer = input("Choose which slot to put your piece player 2 (1-9): ")
            verdict = check_if_valid_input(answer)
            if verdict == True:
                slot = int(answer)
                is_input_valid = True
            

        coords = slot_num(slot,board)
        proceed = check_if_valid(board, coords[0], coords[1], PLAYER2PIECE)
        
        if(check_if_win(board, PLAYER2PIECE)):
            draw_board(board)
            print("Player 2 Won!")
            running = play_again_question()
            board = numpy.zeros(shape = (3,3))
        else:         
            if proceed:
                    turn += 1
        
        